import { inject, injectable } from 'inversify';
import { action, computed, makeObservable, observable } from 'mobx';
import { Types } from '../inversify/inversify.types';
import { SpotifyUserApi } from '../services/api/SpotifyUserApi';
import { SpotifyLocalStorageService } from '../services/SpotifyLocalStorageService';

type SpotifyUser = SpotifyApi.UserObjectPrivate | undefined;

@injectable()
export class UserStore {
	@observable private _user: SpotifyUser;

	@inject(Types.SpotifyUserApi) private spotifyUserApi!: SpotifyUserApi;
	@inject(Types.SpotifyLocalStorageService) private spotifyLocalStorageService!: SpotifyLocalStorageService;

	constructor() {
		makeObservable(this);
	}

	@computed public get user(): SpotifyUser {
		return this._user;
	}

	@action public setUser(user: SpotifyUser): void {
		this._user = user;
	}

	public async fetchUserData(): Promise<void> {
		const res = await this.spotifyUserApi.getMeInfo();

		if (res.isRight()) {
			this.setUser(res.value);
			this.spotifyLocalStorageService.setSpotifyUserId(res.value.id);
		} else {
			this.setUser(undefined);
		}
	}
}
