import { inject, injectable } from 'inversify';
import { computed, makeObservable } from 'mobx';
import { TrackMapper } from '../../helpers/mappers/TrackMapper';
import { Types } from '../../inversify/inversify.types';
import { SpotifySavedTrackListTableApi } from '../../services/api/table/SpotifySavedTrackListTableApi';
import { MSavedTrackObject } from '../../types/SpotifyTypes';
import { BaseSpotifyTableStore } from './BaseSpotifyTableStore';

@injectable()
export class SpotifySavedTrackListTableStore extends BaseSpotifyTableStore<SpotifyApi.SavedTrackObject> {
	@inject(Types.SpotifySavedTrackListTableApi) protected _tableApi!: SpotifySavedTrackListTableApi;

	constructor() {
		super();
		makeObservable(this);
	}

	@computed public get mapSavedTrackObject(): MSavedTrackObject[] | undefined {
		return this._mapTableDataItems(TrackMapper.mapSavedTrackToFullTrack);
	}
}
