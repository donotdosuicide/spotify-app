import { injectable } from 'inversify';
import { action, computed, makeObservable, observable } from 'mobx';
import { SpotifyTableApi } from '../../services/api/table/SpotifyTableApi';
import { PagingData } from '../../types/PagingData';

export interface IInitializable {
	init(): void;
}

@injectable()
export abstract class BaseSpotifyTableStore<T> implements IInitializable {
	protected abstract _tableApi: SpotifyTableApi<T>;

	@observable private _tableData: SpotifyApi.PagingObject<T> | undefined = undefined;

	private readonly defaultPageNumber = 1;
	private readonly defaultPageLimit = 50;

	constructor() {
		makeObservable(this);
	}

	public init(): void {
		this.fetchTableDataByPage();
	}

	public async fetchTableDataByPage(page = this.defaultPageNumber): Promise<void> {
		const offset = (page - 1) * this.defaultPageLimit;
		return this._fetchTableData(this.defaultPageLimit, offset);
	}

	@computed public get tableData(): SpotifyApi.PagingObject<T> | undefined {
		return this._tableData;
	}

	@computed public get pagingData(): PagingData | undefined {
		if (!this._tableData) return undefined;

		return {
			currentPage: Math.ceil(this._tableData?.offset / this._tableData?.limit) + 1,
			pageCount: Math.ceil(this._tableData?.total / this._tableData?.limit),
			pageLimit: this.defaultPageLimit,
			pageSwitcher: (page: number) => this.fetchTableDataByPage(page),
		};
	}

	protected _mapTableDataItems<M>(mapper: (item: T) => M): M[] | undefined {
		return this._tableData?.items.map(mapper);
	}

	private async _fetchTableData(limit = this.defaultPageLimit, offset = 0): Promise<void> {
		const res = await this._tableApi.getTable(limit, offset);

		if (res.isRight()) {
			this._setTableData(res.value);
		} else {
			this._setTableData(undefined);
		}
	}

	@action private _setTableData(tableData: SpotifyApi.PagingObject<T> | undefined): void {
		this._tableData = tableData;
	}
}
