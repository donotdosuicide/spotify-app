import { inject, injectable } from 'inversify';
import { Types } from '../../inversify/inversify.types';
import { SpotifyPlaylistTableApi } from '../../services/api/table/SpotifyPlaylistTableApi';
import { BaseSpotifyTableStore } from './BaseSpotifyTableStore';

@injectable()
export class SpotifyPlaylistTableStore extends BaseSpotifyTableStore<SpotifyApi.PlaylistObjectSimplified> {
	@inject(Types.SpotifyPlaylistTableApi) protected _tableApi!: SpotifyPlaylistTableApi;
}
