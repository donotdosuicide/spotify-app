import { inject, injectable } from 'inversify';
import { computed, makeObservable } from 'mobx';
import { AlbumMapper } from '../../helpers/mappers/AlbumMapper';
import { Types } from '../../inversify/inversify.types';
import { SpotifySavedAlbumListTableApi } from '../../services/api/table/SpotifySavedAlbumListTableApi';
import { MSavedAlbumObject } from '../../types/SpotifyTypes';
import { BaseSpotifyTableStore } from './BaseSpotifyTableStore';

@injectable()
export class SpotifySavedAlbumListTableStore extends BaseSpotifyTableStore<SpotifyApi.SavedAlbumObject> {
	@inject(Types.SpotifySavedAlbumListTableApi) protected _tableApi!: SpotifySavedAlbumListTableApi;

	constructor() {
		super();
		makeObservable(this);
	}

	@computed public get mapSavedAlbumObject(): MSavedAlbumObject[] | undefined {
		return this._mapTableDataItems(AlbumMapper.mapSavedAlbumToFullAlbum);
	}
}
