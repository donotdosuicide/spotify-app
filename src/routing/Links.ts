import { TRACK_LIST_PATH, HOME_PATH, TABLES_PATH, TRACK_TABLE, ALBUM_TABLE, PLAYLIST_TABLE } from './constants';

export interface ILink {
	path: string;
	text: string;
}

export class Links {
	public static get mainLinks(): ILink[] {
		return [
			{
				text: 'Home',
				path: HOME_PATH,
			},
			{
				text: 'TrackList',
				path: TRACK_LIST_PATH,
			},
			{
				text: 'Tables',
				path: TABLES_PATH,
			},
		];
	}

	public static get tableLinks(): ILink[] {
		return [
			{
				text: 'TrackTable',
				path: TRACK_TABLE,
			},
			{
				text: 'AlbumTable',
				path: ALBUM_TABLE,
			},
			{
				text: 'PlaylistTable',
				path: PLAYLIST_TABLE,
			},
		];
	}
}
