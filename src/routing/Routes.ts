import { RouteProps } from 'react-router-dom';
import { AuthCallback } from '../components/AuthCallback';
import { Home } from '../components/pages/Home';
import { AlbumTable } from '../components/pages/tables/content/AlbumTable';
import { PlaylistTable } from '../components/pages/tables/content/PlaylistTable';
import { TrackTable } from '../components/pages/tables/content/TrackTable';
import { TableInfo } from '../components/pages/tables/TableInfo';
import { TablePage } from '../components/pages/tables/TablePage';
import { TableRoot } from '../components/pages/tables/TableRoot';
import { TrackList } from '../components/pages/TrackList';
import {
	ALBUM_TABLE,
	AUTH_CALLBACK_PATH,
	HOME_PATH,
	PLAYLIST_TABLE,
	TABLES_PATH,
	TABLE_PATH,
	TRACK_LIST_PATH,
	TRACK_TABLE,
} from './constants';

export class Routes {
	public static get mainRoutes(): RouteProps[] {
		return [
			{
				component: Home,
				path: HOME_PATH,
				exact: true,
			},
			{
				component: TrackList,
				path: TRACK_LIST_PATH,
			},
			{
				component: TableRoot,
				path: TABLES_PATH,
			},
			{
				component: AuthCallback,
				path: AUTH_CALLBACK_PATH,
			},
		];
	}

	public static get tableRoutes(): RouteProps[] {
		return [
			{
				component: TableInfo,
				path: TABLES_PATH,
				exact: true,
			},
			{
				component: TablePage,
				path: TABLE_PATH,
			},
			{
				component: TrackTable,
				path: TRACK_TABLE,
			},
			{
				component: AlbumTable,
				path: ALBUM_TABLE,
			},
			{
				component: PlaylistTable,
				path: PLAYLIST_TABLE,
			},
		];
	}
}
