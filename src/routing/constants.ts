export const DEV_REDIRECT_PATH = 'http://localhost:3666/auth';

export const HOME_PATH = '/';

export const TRACK_LIST_PATH = '/tracklist';

export const AUTH_CALLBACK_PATH = '/auth';

export const TABLES_PATH = '/tables';
export const TABLE_PATH = `${TABLES_PATH}/:id`;

export const TRACK_TABLE = `${TABLES_PATH}/tracks`;
export const ALBUM_TABLE = `${TABLES_PATH}/albums`;
export const PLAYLIST_TABLE = `${TABLES_PATH}/playlists`;
