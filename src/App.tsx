import { ReactElement, useEffect } from 'react';
import { Router, Switch } from 'react-router-dom';
import './App.scss';
import { LinkList } from './components/LinkList';
import { RouteList } from './components/RouteList';
import { useInject } from './hooks/useInject';
import { Types } from './inversify/inversify.types';
import history from './routing/history';
import { Links } from './routing/Links';
import { Routes } from './routing/Routes';
import { SpotifyWebSdk } from './services/SpotifyWebSdk';

const App = (): ReactElement => {
	const spotifyWebSdk = useInject<SpotifyWebSdk>(Types.SpotifyWebSdk);

	useEffect(() => {
		const initSDK = async () => await spotifyWebSdk.init();
		!spotifyWebSdk.isReady && initSDK();
		return () => spotifyWebSdk.clear();
	}, [spotifyWebSdk]);

	return (
		<div className='App'>
			<Router history={history}>
				<ul className='link-list'>
					<LinkList links={Links.mainLinks} />
				</ul>

				<Switch>
					<RouteList routes={Routes.mainRoutes}></RouteList>
				</Switch>
			</Router>
		</div>
	);
};

export default App;
