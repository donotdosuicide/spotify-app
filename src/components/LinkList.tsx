import { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import { ILink } from '../routing/Links';

interface ILinkListProps {
	links: ILink[];
}

export const LinkList: FunctionComponent<ILinkListProps> = (props) => {
	return (
		<>
			{props.links.map((item) => (
				<li key={item.path}>
					<Link className='App-link' to={item.path}>
						{item.text}
					</Link>
				</li>
			))}
		</>
	);
};
