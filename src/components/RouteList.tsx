import { FunctionComponent } from 'react';
import { Route, RouteProps } from 'react-router-dom';

interface IRouteListProps {
	routes: RouteProps[];
}

export const RouteList: FunctionComponent<IRouteListProps> = (props) => {
	return (
		<>
			{props.routes.map((item) => (
				<Route key={item.path?.toString()} {...item} />
			))}
		</>
	);
};
