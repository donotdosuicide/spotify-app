import { observer } from 'mobx-react';
import { FunctionComponent } from 'react';
import { DateFormatter } from '../../../../helpers/formatters/DateFormatter';
import { SpotifyUriFormatter } from '../../../../helpers/formatters/SpotifyUriFormatter';
import { TimeFormatter } from '../../../../helpers/formatters/TimeFormatter';
import { useInject } from '../../../../hooks/useInject';
import { Types } from '../../../../inversify/inversify.types';
import { SpotifyWebSdk } from '../../../../services/SpotifyWebSdk';
import { SpotifySavedTrackListTableStore } from '../../../../stores/table/SpotifySavedTrackListTableStore';
import { MSavedTrackObject } from '../../../../types/SpotifyTypes';
import { ColumnInfo } from '../../../../types/table/ColumnInfo';
import { TableData } from '../../../../types/table/TableData';
import { nameof } from '../../../../utils/Utils';
import { ButtonCellRenderer } from '../../../shared/table/cell-renderer/ButtonCellRenderer';
import { ImageCellRenderer } from '../../../shared/table/cell-renderer/ImageCellRenderer';
import { TextCellRenderer } from '../../../shared/table/cell-renderer/TextCellRenderer';
import { TableContainer } from '../../../shared/table/TableContainer';

export const TrackTable: FunctionComponent = observer(() => {
	const trackListTableStore = useInject<SpotifySavedTrackListTableStore>(Types.SpotifySavedTrackListTableStore);
	const spotifyWebSdk = useInject<SpotifyWebSdk>(Types.SpotifyWebSdk);

	const getColumnsInfo = (): ColumnInfo<MSavedTrackObject>[] => {
		return [
			{
				title: 'Добавлен',
				key: nameof<MSavedTrackObject>('added_at'),
				renderer: (data) => (
					<TextCellRenderer text={DateFormatter.formatDateToString(new Date(data.added_at))} />
				),
			},
			{
				title: 'Обложка',
				key: nameof<MSavedTrackObject>('album'),
				renderer: (data) => <ImageCellRenderer link={data.album.images[0].url} />,
			},
			{
				title: 'Исполнитель',
				key: nameof<MSavedTrackObject>('artists'),
				renderer: (data) => <TextCellRenderer text={data.artists.map((a) => a.name).join(', ')} />,
			},
			{
				title: 'Трек',
				key: nameof<MSavedTrackObject>('name'),
				renderer: (data) => <TextCellRenderer text={data.name} />,
			},
			{
				title: 'Альбом',
				key: nameof<MSavedTrackObject>('album'),
				renderer: (data) => <TextCellRenderer text={data.album.name} />,
			},
			{
				title: 'Длительность',
				key: nameof<MSavedTrackObject>('duration_ms'),
				renderer: (data) => (
					<TextCellRenderer text={TimeFormatter.formatMillisecondsToTimeDuration(data.duration_ms)} />
				),
			},
			{
				title: '',
				key: nameof<MSavedTrackObject>('id'),
				renderer: (data) => (
					<ButtonCellRenderer
						text={'>'}
						onClick={() => spotifyWebSdk.play([SpotifyUriFormatter.getUri(data.id, 'track')])}
					/>
				),
			},
		];
	};

	const tableData: TableData<MSavedTrackObject> = {
		columnsInfo: getColumnsInfo(),
		list: trackListTableStore.mapSavedTrackObject,
	};

	return <TableContainer title='Track Table' data={tableData} tableStore={trackListTableStore} paginationEnabled />;
});
