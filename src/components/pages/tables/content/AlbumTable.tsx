import { observer } from 'mobx-react';
import { FunctionComponent } from 'react';
import { DateFormatter } from '../../../../helpers/formatters/DateFormatter';
import { useInject } from '../../../../hooks/useInject';
import { Types } from '../../../../inversify/inversify.types';
import { SpotifyWebSdk } from '../../../../services/SpotifyWebSdk';
import { SpotifySavedAlbumListTableStore } from '../../../../stores/table/SpotifySavedAlbumListTableStore';
import { MSavedAlbumObject } from '../../../../types/SpotifyTypes';
import { ColumnInfo } from '../../../../types/table/ColumnInfo';
import { TableData } from '../../../../types/table/TableData';
import { nameof } from '../../../../utils/Utils';
import { ButtonCellRenderer } from '../../../shared/table/cell-renderer/ButtonCellRenderer';
import { ImageCellRenderer } from '../../../shared/table/cell-renderer/ImageCellRenderer';
import { TextCellRenderer } from '../../../shared/table/cell-renderer/TextCellRenderer';
import { TableContainer } from '../../../shared/table/TableContainer';

export const AlbumTable: FunctionComponent = observer(() => {
	const albumListTableStore = useInject<SpotifySavedAlbumListTableStore>(Types.SpotifySavedAlbumListTableStore);
	const spotifyWebSdk = useInject<SpotifyWebSdk>(Types.SpotifyWebSdk);

	const getColumnsInfo = (): ColumnInfo<MSavedAlbumObject>[] => {
		return [
			{
				title: 'Дата выхода',
				key: nameof<MSavedAlbumObject>('release_date'),
				renderer: (data) => (
					<TextCellRenderer text={DateFormatter.formatDateToString(new Date(data.release_date))} />
				),
			},
			{
				title: 'Обложка',
				key: nameof<MSavedAlbumObject>('images'),
				renderer: (data) => <ImageCellRenderer link={data.images[0].url} />,
			},

			{
				title: 'Название',
				key: nameof<MSavedAlbumObject>('added_at'),
				renderer: (data) => <TextCellRenderer text={data.name} />,
			},
			{
				title: 'Исполнитель',
				key: nameof<MSavedAlbumObject>('artists'),
				renderer: (data) => <TextCellRenderer text={data.artists.map((a) => a.name).join(', ')} />,
			},
			{
				title: 'Жанры',
				key: nameof<MSavedAlbumObject>('genres'),
				renderer: (data) => <TextCellRenderer text={data.genres.join(', ')} />,
			},
			{
				title: 'Популярность',
				key: nameof<MSavedAlbumObject>('popularity'),
				renderer: (data) => <TextCellRenderer text={data.popularity.toString()} />,
			},
			{
				title: 'Добавлен',
				key: nameof<MSavedAlbumObject>('added_at'),
				renderer: (data) => (
					<TextCellRenderer text={DateFormatter.formatDateToString(new Date(data.added_at))} />
				),
			},
			{
				title: '',
				key: nameof<MSavedAlbumObject>('uri'),
				renderer: (data) => <ButtonCellRenderer text={'>'} onClick={() => spotifyWebSdk.play(data.uri)} />,
			},
		];
	};

	const tableData: TableData<MSavedAlbumObject> = {
		columnsInfo: getColumnsInfo(),
		list: albumListTableStore.mapSavedAlbumObject,
	};

	return <TableContainer title='Album Table' data={tableData} tableStore={albumListTableStore} paginationEnabled />;
});
