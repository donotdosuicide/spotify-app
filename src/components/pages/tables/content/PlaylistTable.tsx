import { observer } from 'mobx-react';
import { FunctionComponent } from 'react';
import { useInject } from '../../../../hooks/useInject';
import { Types } from '../../../../inversify/inversify.types';
import { SpotifyWebSdk } from '../../../../services/SpotifyWebSdk';
import { SpotifyPlaylistTableStore } from '../../../../stores/table/SpotifyPlaylistTableStore';
import { ColumnInfo } from '../../../../types/table/ColumnInfo';
import { TableData } from '../../../../types/table/TableData';
import { nameof } from '../../../../utils/Utils';
import { ButtonCellRenderer } from '../../../shared/table/cell-renderer/ButtonCellRenderer';
import { ImageCellRenderer } from '../../../shared/table/cell-renderer/ImageCellRenderer';
import { TextCellRenderer } from '../../../shared/table/cell-renderer/TextCellRenderer';
import { TableContainer } from '../../../shared/table/TableContainer';

export const PlaylistTable: FunctionComponent = observer(() => {
	const spotifyPlaylistTableStore = useInject<SpotifyPlaylistTableStore>(Types.SpotifyPlaylistTableStore);
	const spotifyWebSdk = useInject<SpotifyWebSdk>(Types.SpotifyWebSdk);

	const getColumnsInfo = (): ColumnInfo<SpotifyApi.PlaylistObjectSimplified>[] => {
		return [
			{
				title: 'Название',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('name'),
				renderer: (data) => <TextCellRenderer text={data.name} />,
			},
			{
				title: 'Обложка',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('images'),
				renderer: (data) => <ImageCellRenderer link={data.images[0].url || ''} />,
			},
			{
				title: 'Количество треков',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('tracks'),
				renderer: (data) => <TextCellRenderer text={data.tracks.total.toString() || '0'} />,
			},
			{
				title: 'Создатель',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('owner'),
				renderer: (data) => <TextCellRenderer text={data.owner.display_name || ''} />,
			},
			{
				title: 'Приватность',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('public'),
				renderer: (data) => <TextCellRenderer text={data.public ? 'Публичный' : 'Частный'} />,
			},
			{
				title: '',
				key: nameof<SpotifyApi.PlaylistObjectSimplified>('uri'),
				renderer: (data) => <ButtonCellRenderer text={'>'} onClick={() => spotifyWebSdk.play(data.uri)} />,
			},
		];
	};

	const tableData: TableData<SpotifyApi.PlaylistObjectSimplified> = {
		columnsInfo: getColumnsInfo(),
		list: spotifyPlaylistTableStore.tableData?.items || [],
	};

	return (
		<TableContainer
			title='Playlist Table'
			data={tableData}
			tableStore={spotifyPlaylistTableStore}
			paginationEnabled
		/>
	);
});
