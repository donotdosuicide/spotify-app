import { FunctionComponent } from 'react';
import { Switch } from 'react-router-dom';
import { Links } from '../../../routing/Links';
import { Routes } from '../../../routing/Routes';
import { LinkList } from '../../LinkList';
import { RouteList } from '../../RouteList';

export const TableRoot: FunctionComponent = () => {
	return (
		<div>
			<h3>Tables</h3>

			<ul className='link-list'>
				<LinkList links={Links.tableLinks} />
			</ul>

			<Switch>
				<RouteList routes={Routes.tableRoutes} />
			</Switch>
		</div>
	);
};
