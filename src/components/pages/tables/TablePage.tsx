import { FunctionComponent } from 'react';
import { Redirect, useParams } from 'react-router-dom';
import { TABLES_PATH } from '../../../routing/constants';

export const TablePage: FunctionComponent = () => {
	const { id } = useParams<{ id: string }>();

	return <Redirect to={`${TABLES_PATH}/${id}`} />;
};
