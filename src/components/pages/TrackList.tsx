import { observer } from 'mobx-react';
import { FunctionComponent, useEffect } from 'react';
import { SpotifyUriFormatter } from '../../helpers/formatters/SpotifyUriFormatter';
import { useInject } from '../../hooks/useInject';
import { Types } from '../../inversify/inversify.types';
import { SpotifyWebSdk } from '../../services/SpotifyWebSdk';
import { SpotifySavedTrackListTableStore } from '../../stores/table/SpotifySavedTrackListTableStore';

export const TrackList: FunctionComponent = observer(() => {
	const trackListTableStore = useInject<SpotifySavedTrackListTableStore>(Types.SpotifySavedTrackListTableStore);
	const spotifyWebSdk = useInject<SpotifyWebSdk>(Types.SpotifyWebSdk);

	useEffect(() => {
		trackListTableStore.init();
	}, [trackListTableStore]);

	return (
		<>
			<h3>TrackList</h3>

			<div className='playlist'>
				{trackListTableStore.mapSavedTrackObject &&
					trackListTableStore.mapSavedTrackObject.map((item) => (
						<div
							key={item.id}
							className='track'
							onClick={() => spotifyWebSdk.play([SpotifyUriFormatter.getUri(item.id, 'track')])}
						>
							<img src={item.album.images[0].url} alt={item.name} />
							<span>
								{item.artists.map((a) => a.name).join(',')} - {item.name}
							</span>
							<span>{item.duration_ms / 1000} s</span>
						</div>
					))}
			</div>
		</>
	);
});
