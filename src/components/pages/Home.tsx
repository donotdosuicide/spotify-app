import { observer } from 'mobx-react';
import { FunctionComponent, useEffect } from 'react';
import { useInject } from '../../hooks/useInject';
import { Types } from '../../inversify/inversify.types';
import { AuthApi } from '../../services/api/AuthApi';
import { AuthApiService } from '../../services/api/AuthApiService';
import { UserStore } from '../../stores/UserStore';

export const Home: FunctionComponent = observer(() => {
	const authApi = useInject<AuthApi>(Types.AuthApi);
	const authApiService = useInject<AuthApiService>(Types.AuthApiService);
	const userStore = useInject<UserStore>(Types.UserStore);

	useEffect(() => {
		if (authApiService.isNeedToUpdateToken()) {
			authApi.authorize();
		}

		userStore.fetchUserData();
	}, [authApi, authApiService, userStore]);

	return (
		<div>
			<h3>Home</h3>
			<pre>{JSON.stringify(userStore.user, undefined, 2)}</pre>
		</div>
	);
});
