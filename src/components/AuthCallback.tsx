import { FunctionComponent, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useInject } from '../hooks/useInject';
import { Types } from '../inversify/inversify.types';
import { HOME_PATH } from '../routing/constants';
import history from '../routing/history';
import { AuthApiService } from '../services/api/AuthApiService';

export const AuthCallback: FunctionComponent = () => {
	const location = useLocation();
	const authApiService = useInject<AuthApiService>(Types.AuthApiService);

	useEffect(() => {
		if (!location.hash) {
			return;
		}

		authApiService.parseAuthorizeHashAndSaveFields(location.hash);
		history.push(HOME_PATH);
	}, [location, authApiService]);

	return (
		<div>
			<h2>AuthCallback - see console output</h2>
		</div>
	);
};
