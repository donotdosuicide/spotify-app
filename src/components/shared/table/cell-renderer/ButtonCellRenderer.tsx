export interface IButtonCellRendererProps {
	text: string;
	onClick: () => void;
}

export const ButtonCellRenderer = (props: IButtonCellRendererProps): JSX.Element => {
	return <button onClick={props.onClick}>{props.text ? props.text : 'NO DATA'}</button>;
};
