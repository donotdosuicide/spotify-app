export interface ITextCellRendererProps {
	text: string;
}

export const TextCellRenderer = (props: ITextCellRendererProps): JSX.Element => {
	return <>{props.text ? props.text : 'NO DATA'}</>;
};
