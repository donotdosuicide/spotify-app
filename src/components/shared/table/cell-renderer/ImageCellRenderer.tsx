export interface IImageCellRendererProps {
	link: string;
}

export const ImageCellRenderer = (props: IImageCellRendererProps): JSX.Element => {
	return <img src={props.link} alt={props.link} />;
};
