import { observer } from 'mobx-react';
import { ReactElement, useEffect } from 'react';
import { BaseSpotifyTableStore } from '../../../stores/table/BaseSpotifyTableStore';
import { TableData } from '../../../types/table/TableData';
import { TableBody } from './table-details/TableBody';
import { TableHead } from './table-details/TableHead';
import { TableWrapper } from './table-details/TableWrapper';
export interface ITableProps<T, Mapped> {
	tableStore: BaseSpotifyTableStore<T>;
	data: TableData<Mapped>;
}

export const Table = observer(<T, M>(props: ITableProps<T, M>): ReactElement => {
	useEffect(() => {
		props.tableStore.init();
	}, [props.tableStore]);

	if (!props.data.list || !props.data.list.length) {
		return <div>{'Информации по данному запросу пока нет'}</div>;
	}

	return (
		<TableWrapper>
			<TableHead columnsInfo={props.data.columnsInfo} />
			<TableBody data={props.data} />
		</TableWrapper>
	);
});
