import classNames from 'classnames';
import { FunctionComponent } from 'react';
import { PagingData } from '../../../types/PagingData';

interface IPaginationProps {
	pagingData: PagingData;
}

export const Pagination: FunctionComponent<IPaginationProps> = (props) => {
	const nearVisibleBtnCount = 4;

	const current = props.pagingData.currentPage;
	const pageCount = props.pagingData.pageCount;

	const leftBorder = current - nearVisibleBtnCount <= 0 ? 1 : current - nearVisibleBtnCount;
	const rightBorder = current + nearVisibleBtnCount >= pageCount ? pageCount : current + nearVisibleBtnCount;

	const getButton = (idx: number, additionalClassName?: string): JSX.Element => (
		<button
			className={classNames(
				'page-button',
				idx === current ? 'page-button_active' : undefined,
				additionalClassName ? additionalClassName : undefined,
			)}
			key={idx}
			onClick={() => props.pagingData.pageSwitcher(idx)}
		>
			{idx}
		</button>
	);

	const getButtons = (): JSX.Element[] => {
		const buttons: JSX.Element[] = [];

		if (current - nearVisibleBtnCount > 1) {
			buttons.push(getButton(1, 'page-button_bordered_left'));
		}

		for (let i = leftBorder; i <= rightBorder; i++) {
			buttons.push(getButton(i));
		}

		if (pageCount - current > nearVisibleBtnCount) {
			buttons.push(getButton(pageCount, 'page-button_bordered_right'));
		}

		return buttons;
	};

	return <div className='pagination'>{getButtons()}</div>;
};
