import { ReactElement } from 'react';
import { TableData } from '../../../../types/table/TableData';
import { TableRow } from './TableRow';

interface ITableBodyProps<T> {
	data: TableData<T>;
}

export const TableBody = <T,>(props: ITableBodyProps<T>): ReactElement => {
	return (
		<tbody>
			{props.data.list &&
				props.data.list.map((listEl, rowIdx) => (
					<TableRow key={rowIdx} columnsInfo={props.data.columnsInfo} rowIdx={rowIdx} listElement={listEl} />
				))}
		</tbody>
	);
};
