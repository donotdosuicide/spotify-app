import { ReactElement } from 'react';
import { ColumnInfo } from '../../../../types/table/ColumnInfo';

interface ITableHeadProps<T> {
	columnsInfo: ColumnInfo<T>[];
}

export const TableHead = <T,>(props: ITableHeadProps<T>): ReactElement => {
	return (
		<thead>
			<tr>
				{props.columnsInfo.map((el) => (
					<th key={el.title}>{el.title}</th>
				))}
			</tr>
		</thead>
	);
};
