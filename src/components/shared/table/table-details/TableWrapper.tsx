import { ReactElement, ReactNode } from 'react';

interface ITableWrapperProps {
	children: ReactNode;
}

export const TableWrapper = (props: ITableWrapperProps): ReactElement => {
	return <table>{props.children}</table>;
};
