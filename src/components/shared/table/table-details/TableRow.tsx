import { ReactElement } from 'react';
import { ColumnInfo } from '../../../../types/table/ColumnInfo';
interface ITableRowProps<T> {
	rowIdx: number;
	listElement: T;
	columnsInfo: ColumnInfo<T>[];
}

export const TableRow = <T,>(props: ITableRowProps<T>): ReactElement => {
	const getTdKey = (colKey: string, colIdx: number) => `${props.rowIdx}:${colIdx}:${colKey}`;
	const getIn = (listEl: T, key: keyof T) => listEl[key];
	return (
		<tr>
			{props.columnsInfo.map((col, colIdx) => (
				<td key={getTdKey(col.key.toString(), colIdx)}>
					{col.renderer
						? col.renderer(props.listElement)
						: col.valueGetter
						? col.valueGetter(props.listElement)
						: getIn(props.listElement, col.key)}
				</td>
			))}
		</tr>
	);
};
