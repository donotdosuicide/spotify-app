import { observer } from 'mobx-react';
import { ReactElement } from 'react';
import { Pagination } from './Pagination';
import { ITableProps, Table } from './Table';

interface ITableContainerProps<T, Mapped> extends ITableProps<T, Mapped> {
	title: string;
	paginationEnabled?: boolean;
}

export const TableContainer = observer(<T, M>(props: ITableContainerProps<T, M>): ReactElement => {
	return (
		<>
			<h3>{props.title}</h3>
			<div className='table-container'>
				<Table data={props.data} tableStore={props.tableStore} />
			</div>
			{props.paginationEnabled && props.tableStore.pagingData && (
				<Pagination pagingData={props.tableStore.pagingData} />
			)}
		</>
	);
});
