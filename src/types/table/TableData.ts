import { ColumnInfo } from './ColumnInfo';

export interface TableData<T> {
	columnsInfo: ColumnInfo<T>[];
	list: T[] | undefined;
}
