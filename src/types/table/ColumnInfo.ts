export interface ColumnInfo<TData> {
	title: string;
	key: keyof TData;
	renderer: (data: TData) => JSX.Element;
	filterEnabled?: boolean;
	valueGetter?: (data: TData) => TData[keyof TData];
}
