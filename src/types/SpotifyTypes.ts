import { AxiosResponse } from 'axios';

export type SpotifyAuthHash = {
	access_token: string;
	token_type: string;
	expires_in: string;
	state: string;
};

export type SpotifyError = AxiosResponse<SpotifyApi.ErrorObject>;

export type MSavedTrackObject = Omit<SpotifyApi.SavedTrackObject, 'track'> & SpotifyApi.TrackObjectFull;

export type MSavedAlbumObject = Omit<SpotifyApi.SavedAlbumObject, 'album'> & SpotifyApi.AlbumObjectFull;
