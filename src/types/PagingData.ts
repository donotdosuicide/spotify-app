export interface PagingData {
	currentPage: number;
	pageCount: number;
	pageLimit: number;
	pageSwitcher: (page: number) => Promise<void>;
}
