import { Container } from 'inversify';
/*  !!! reflect-metadata should be imported before any interface or other imports
    also it should be imported only once so that a singleton is created !!! */
import 'reflect-metadata';
import { ApiUrlFabric } from '../services/api/ApiUrlFabric';
import { AuthApi } from '../services/api/AuthApi';
import { AuthApiService } from '../services/api/AuthApiService';
import { SpotifyPlayerApi } from '../services/api/SpotifyPlayerApi';
import { SpotifyUserApi } from '../services/api/SpotifyUserApi';
import { SpotifyPlaylistTableApi } from '../services/api/table/SpotifyPlaylistTableApi';
import { SpotifySavedAlbumListTableApi } from '../services/api/table/SpotifySavedAlbumListTableApi';
import { SpotifySavedTrackListTableApi } from '../services/api/table/SpotifySavedTrackListTableApi';
import { SpotifyLocalStorageService } from '../services/SpotifyLocalStorageService';
import { SpotifyWebSdk } from '../services/SpotifyWebSdk';
import { SpotifyPlaylistTableStore } from '../stores/table/SpotifyPlaylistTableStore';
import { SpotifySavedAlbumListTableStore } from '../stores/table/SpotifySavedAlbumListTableStore';
import { SpotifySavedTrackListTableStore } from '../stores/table/SpotifySavedTrackListTableStore';
import { UserStore } from '../stores/UserStore';
import { Types } from './inversify.types';

const container = new Container();

// apis
container.bind<ApiUrlFabric>(Types.ApiUrlFabric).to(ApiUrlFabric);
container.bind<AuthApi>(Types.AuthApi).to(AuthApi);
container.bind<SpotifyUserApi>(Types.SpotifyUserApi).to(SpotifyUserApi);
container.bind<SpotifyPlayerApi>(Types.SpotifyPlayerApi).to(SpotifyPlayerApi);
container.bind<SpotifySavedTrackListTableApi>(Types.SpotifySavedTrackListTableApi).to(SpotifySavedTrackListTableApi);
container.bind<SpotifySavedAlbumListTableApi>(Types.SpotifySavedAlbumListTableApi).to(SpotifySavedAlbumListTableApi);
container.bind<SpotifyPlaylistTableApi>(Types.SpotifyPlaylistTableApi).to(SpotifyPlaylistTableApi);
// apis end

// services
container.bind<AuthApiService>(Types.AuthApiService).to(AuthApiService);
container.bind<SpotifyWebSdk>(Types.SpotifyWebSdk).to(SpotifyWebSdk).inSingletonScope();
container
	.bind<SpotifyLocalStorageService>(Types.SpotifyLocalStorageService)
	.to(SpotifyLocalStorageService)
	.inSingletonScope();
// services end

// stores
container.bind<UserStore>(Types.UserStore).to(UserStore).inSingletonScope();
container
	.bind<SpotifySavedTrackListTableStore>(Types.SpotifySavedTrackListTableStore)
	.to(SpotifySavedTrackListTableStore)
	.inSingletonScope();
container
	.bind<SpotifySavedAlbumListTableStore>(Types.SpotifySavedAlbumListTableStore)
	.to(SpotifySavedAlbumListTableStore)
	.inSingletonScope();
container
	.bind<SpotifyPlaylistTableStore>(Types.SpotifyPlaylistTableStore)
	.to(SpotifyPlaylistTableStore)
	.inSingletonScope();
// stores end

export default container;
