export const Types = {
	ApiUrlFabric: Symbol('ApiUrlFabric'),
	AuthApi: Symbol('AuthApi'),
	SpotifyUserApi: Symbol('SpotifyUserApi'),
	SpotifyPlayerApi: Symbol('SpotifyPlayerApi'),
	SpotifySavedTrackListTableApi: Symbol('SpotifySavedTrackListTableApi'),
	SpotifySavedAlbumListTableApi: Symbol('SpotifySavedAlbumListTableApi'),
	SpotifyPlaylistTableApi: Symbol('SpotifyPlaylistTableApi'),

	AuthApiService: Symbol('AuthApiService'),
	SpotifyWebSdk: Symbol('SpotifyWebSdk'),
	SpotifyLocalStorageService: Symbol('SpotifyLocalStorageService'),

	UserStore: Symbol('UserStore'),
	SpotifySavedTrackListTableStore: Symbol('SpotifySavedTrackListTableStore'),
	SpotifySavedAlbumListTableStore: Symbol('SpotifySavedAlbumListTableStore'),
	SpotifyPlaylistTableStore: Symbol('SpotifyPlaylistTableStore'),
};
