export class StringFormatter {
	public static getJsonFromUrl(url: string): Record<string, string> {
		const result: Record<string, string> = {};

		url.split('&').forEach((part) => {
			const item = part.split('=');
			result[item[0]] = decodeURIComponent(item[1]);
		});

		return result;
	}
}
