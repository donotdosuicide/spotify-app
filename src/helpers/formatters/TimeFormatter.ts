export class TimeFormatter {
	public static formatMillisecondsToTimeDuration(ms: number, divider = ':'): string {
		const secondsFullTime = Math.ceil(ms / 1000);
		const minutes = Math.floor(secondsFullTime / 60);
		const restSeconds = (secondsFullTime % 60).toString();

		const restSecondsOut = restSeconds.length === 1 ? `0${restSeconds}` : restSeconds;

		return `${minutes}${divider}${restSecondsOut}`;
	}
}
