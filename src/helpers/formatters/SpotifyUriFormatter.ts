export class SpotifyUriFormatter {
	public static getUri(id: string, type: 'track' | 'album'): string {
		return `spotify:${type}:${id}`;
	}
}
