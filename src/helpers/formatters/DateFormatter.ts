export class DateFormatter {
	public static formatDateToString(d: Date, divider = '.'): string {
		const isoString = d.toISOString();
		const [date] = isoString.split('T');
		const [year, day, month] = date.split('-');

		return `${day}${divider}${month}${divider}${year}`;
	}
}
