import { MSavedTrackObject } from '../../types/SpotifyTypes';

export class TrackMapper {
	public static mapSavedTrackToFullTrack(savedTrack: SpotifyApi.SavedTrackObject): MSavedTrackObject {
		return { added_at: savedTrack.added_at, ...savedTrack.track };
	}
}
