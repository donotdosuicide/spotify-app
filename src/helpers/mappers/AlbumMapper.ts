import { MSavedAlbumObject } from '../../types/SpotifyTypes';

export class AlbumMapper {
	public static mapSavedAlbumToFullAlbum(savedAlbum: SpotifyApi.SavedAlbumObject): MSavedAlbumObject {
		return { added_at: savedAlbum.added_at, ...savedAlbum.album };
	}
}
