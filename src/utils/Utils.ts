export const nameof = <T>(name: keyof T): keyof T => name;

export const nestedNameOf = <T, P>(name: keyof T, nestedName: keyof P): string => {
	return `${name}.${nestedName}`;
};
