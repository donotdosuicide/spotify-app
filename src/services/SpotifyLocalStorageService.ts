import { injectable } from 'inversify';

@injectable()
export class SpotifyLocalStorageService {
	private readonly spotifyAuthStateKey = 'SpotifyAuthState';
	private readonly spotifyAuthAccessTokenKey = 'SpotifyAuthAccessToken';
	private readonly spotifyAuthAccessTokenTypeKey = 'SpotifyAuthAccessTokenType';
	private readonly spotifyAuthExpiresInKey = 'SpotifyAuthExpiresIn';

	private readonly spotifyUserIdKey = 'spotifyUserIdKey';
	private readonly spotifyDeviceIdKey = 'spotifyDeviceIdKey';

	public setSpotifyAuthState(state: string): void {
		localStorage.setItem(this.spotifyAuthStateKey, state);
	}

	public getSpotifyAuthState(): string | null {
		return localStorage.getItem(this.spotifyAuthStateKey);
	}

	public setSpotifyAuthAccessToken(token: string): void {
		localStorage.setItem(this.spotifyAuthAccessTokenKey, token);
	}

	public getSpotifyAuthAccessToken(): string | null {
		return localStorage.getItem(this.spotifyAuthAccessTokenKey);
	}

	public setSpotifyAuthAccessTokenType(type: string): void {
		localStorage.setItem(this.spotifyAuthAccessTokenTypeKey, type);
	}

	public getSpotifyAuthAccessTokenType(): string | null {
		return localStorage.getItem(this.spotifyAuthAccessTokenTypeKey);
	}

	public setSpotifyAuthExpiresIn(expiresTimeInMilliseconds: string): void {
		localStorage.setItem(this.spotifyAuthExpiresInKey, expiresTimeInMilliseconds);
	}

	public getSpotifyAuthExpiresIn(): string | null {
		return localStorage.getItem(this.spotifyAuthExpiresInKey);
	}

	public clearAuthData(): void {
		localStorage.removeItem(this.spotifyAuthAccessTokenKey);
		localStorage.removeItem(this.spotifyAuthAccessTokenTypeKey);
		localStorage.removeItem(this.spotifyAuthExpiresInKey);
		localStorage.removeItem(this.spotifyAuthStateKey);
	}

	public setSpotifyUserId(id: string | undefined): void {
		localStorage.setItem(this.spotifyUserIdKey, id || '');
	}

	public getSpotifyUserId(): string | null {
		return localStorage.getItem(this.spotifyUserIdKey);
	}

	public setSpotifyDeviceId(id: string | undefined): void {
		localStorage.setItem(this.spotifyDeviceIdKey, id || '');
	}

	public getSpotifyDeviceId(): string | null {
		return localStorage.getItem(this.spotifyDeviceIdKey);
	}
}
