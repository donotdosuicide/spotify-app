import { injectable, inject } from 'inversify';
import { StringFormatter } from '../../helpers/formatters/StringFormatter';
import { Types } from '../../inversify/inversify.types';
import { SpotifyAuthHash } from '../../types/SpotifyTypes';
import { SpotifyLocalStorageService } from '../SpotifyLocalStorageService';
@injectable()
export class AuthApiService {
	@inject(Types.SpotifyLocalStorageService) private _spotifyLocalStorageService!: SpotifyLocalStorageService;

	public parseAuthorizeHashAndSaveFields(hash: string): void {
		const res = StringFormatter.getJsonFromUrl(hash.slice(1)) as SpotifyAuthHash;

		this._spotifyLocalStorageService.setSpotifyAuthAccessToken(res.access_token);
		this._spotifyLocalStorageService.setSpotifyAuthAccessTokenType(res.token_type);
		this._spotifyLocalStorageService.setSpotifyAuthState(res.state);

		this._formatAndSetSpotifyAuthExpiresIn(res.expires_in);
	}

	public isNeedToUpdateToken(): boolean {
		const token = this._spotifyLocalStorageService.getSpotifyAuthAccessToken();

		if (!token) {
			return true;
		}

		const now = new Date().getTime();
		const expiresIn = this._spotifyLocalStorageService.getSpotifyAuthExpiresIn() || 0;

		return now > +expiresIn;
	}

	private _formatAndSetSpotifyAuthExpiresIn(expiresLifeTimeInSeconds: string): void {
		const lifeTimeInMilliseconds = +expiresLifeTimeInSeconds * 1000 * 0.9;
		const expiresTimeInMilliseconds = new Date().getTime() + lifeTimeInMilliseconds;

		this._spotifyLocalStorageService.setSpotifyAuthExpiresIn(expiresTimeInMilliseconds.toString());
	}
}
