import { Either } from '@sweet-monads/either';
import { injectable } from 'inversify';
import { SpotifyError } from '../../types/SpotifyTypes';
import { AxiosApi } from './base/AxiosApi';

@injectable()
export class SpotifyUserApi extends AxiosApi {
	public getMeInfo(): Promise<Either<SpotifyError, SpotifyApi.UserObjectPrivate>> {
		const req = this._get<SpotifyApi.UserObjectPrivate>({ url: this._apiUrlFabric.me });

		return this._doApiRequest(req);
	}
}
