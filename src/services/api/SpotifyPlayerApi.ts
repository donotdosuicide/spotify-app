import { Either } from '@sweet-monads/either';
import { injectable } from 'inversify';
import { SpotifyError } from '../../types/SpotifyTypes';
import { AxiosApi } from './base/AxiosApi';

@injectable()
export class SpotifyPlayerApi extends AxiosApi {
	public play(data: SpotifyApi.PlayParameterObject): Promise<Either<SpotifyError, void>> {
		const deviceId = this._spotifyLocalStorageService.getSpotifyDeviceId() || '';

		const req = this._put<void>({ url: this._apiUrlFabric.play(deviceId), payload: data });

		return this._doApiRequest(req);
	}
}
