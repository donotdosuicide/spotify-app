export interface RequestData<ConfigType, PayloadType = unknown> {
	url: string;
	config?: ConfigType;
	payload?: PayloadType;
}

export interface ApiMethod<ConfigType> {
	<DataType>(data: RequestData<ConfigType>): Promise<DataType>;
}

export abstract class BaseApi<RequestConfigType> {
	protected abstract _get: ApiMethod<RequestConfigType>;
	protected abstract _post: ApiMethod<RequestConfigType>;
	protected abstract _put: ApiMethod<RequestConfigType>;
	protected abstract _delete: ApiMethod<RequestConfigType>;
}
