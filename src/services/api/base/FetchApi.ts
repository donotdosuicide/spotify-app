import { injectable } from 'inversify';
import { BaseApi, RequestData } from './BaseApi';

// export type FetchResponse<T> = Response & {
// 	jsonData: T;
// };

@injectable()
export class FetchApi extends BaseApi<RequestInit> {
	// protected _get = async <T, R = FetchResponse<T>>(data: RequestData<RequestInit>): Promise<R> => {
	// 	const res = await fetch(data.url, { ...data.config, method: 'GET' });
	// 	const json = await res.json();

	// 	const fullRes: FetchResponse<T> = { jsonData: json, ...res };
	// 	return fullRes;
	// };

	protected _get = async <T>(data: RequestData<RequestInit>): Promise<T> => {
		const res = await fetch(data.url, { ...data.config, method: 'GET' });
		return res.json();
	};

	protected _post = async <T>(data: RequestData<RequestInit>): Promise<T> => {
		const res = await fetch(data.url, { ...data.config, method: 'POST', body: data.payload as BodyInit });
		return res.json();
	};

	protected _put = async <T>(data: RequestData<RequestInit>): Promise<T> => {
		const res = await fetch(data.url, { ...data.config, method: 'PUT', body: data.payload as BodyInit });
		return res.json();
	};

	protected _delete = async <T>(data: RequestData<RequestInit>): Promise<T> => {
		const res = await fetch(data.url, { ...data.config, method: 'DELETE' });
		return res.json();
	};
}
