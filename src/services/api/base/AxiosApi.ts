import { Either, left, right } from '@sweet-monads/either';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { inject, injectable } from 'inversify';
import { Types } from '../../../inversify/inversify.types';
import { HOME_PATH } from '../../../routing/constants';
import history from '../../../routing/history';
import { SpotifyError } from '../../../types/SpotifyTypes';
import { SpotifyLocalStorageService } from '../../SpotifyLocalStorageService';
import { ApiUrlFabric } from '../ApiUrlFabric';
import { BaseApi, RequestData } from './BaseApi';

@injectable()
export class AxiosApi extends BaseApi<AxiosRequestConfig> {
	@inject(Types.ApiUrlFabric) protected _apiUrlFabric!: ApiUrlFabric;
	@inject(Types.SpotifyLocalStorageService) protected _spotifyLocalStorageService!: SpotifyLocalStorageService;

	constructor() {
		super();

		axios.interceptors.request.use(
			(config) => this._interceptOnFulfilled(config),
			(e) => this._interceptOnRejected(e),
		);
	}

	protected _doApiRequest = async <TBody, TError = SpotifyError>(
		apiRequest: Promise<AxiosResponse<TBody>>,
	): Promise<Either<AxiosResponse<TError>, TBody>> => {
		try {
			const response = await apiRequest;
			return right(response.data);
		} catch (e) {
			this._logError(e);
			this._handleErrorCode(e.response);

			return left(e.response);
		}
	};

	protected _get = <T, R = AxiosResponse<T>>(data: RequestData<AxiosRequestConfig>): Promise<R> => {
		return axios.get<T, R>(data.url, data.config);
	};

	protected _post = <T, R = AxiosResponse<T>>(data: RequestData<AxiosRequestConfig>): Promise<R> => {
		return axios.post(data.url, data.payload, data.config);
	};

	protected _put = <T, R = AxiosResponse<T>>(data: RequestData<AxiosRequestConfig>): Promise<R> => {
		return axios.put(data.url, data.payload, data.config);
	};

	protected _delete = <T, R = AxiosResponse<T>>(data: RequestData<AxiosRequestConfig>): Promise<R> => {
		return axios.delete(data.url);
	};

	private _interceptOnFulfilled(config: AxiosRequestConfig) {
		const token = this._spotifyLocalStorageService.getSpotifyAuthAccessToken();
		const tokenType = this._spotifyLocalStorageService.getSpotifyAuthAccessTokenType();

		if (token && tokenType) {
			config.headers['Authorization'] = `${tokenType} ${token}`;
		}

		return config;
	}

	private _interceptOnRejected(error: unknown): Promise<never> {
		console.error(error);
		return Promise.reject(error);
	}

	private _handleErrorCode(error: AxiosResponse): void {
		const statusCode = error.status;

		switch (statusCode) {
			case 401:
				this._spotifyLocalStorageService.clearAuthData();
				history.push(HOME_PATH);
				break;

			default:
				console.log('Unregistered error code:', statusCode);
				break;
		}
	}

	private _logError(e: { response: unknown }): void {
		console.error(e);
		console.log('doApiRequest catch error', e.response);
	}
}
