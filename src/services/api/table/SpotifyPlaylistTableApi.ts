import { getContainer } from '../../../inversify/inversify.getContainer';
import { Types } from '../../../inversify/inversify.types';
import { ApiUrlFabric } from '../ApiUrlFabric';
import { SpotifyTableApi } from './SpotifyTableApi';

export class SpotifyPlaylistTableApi extends SpotifyTableApi<SpotifyApi.PlaylistObjectSimplified> {
	protected _tableUrl = getContainer<ApiUrlFabric>(Types.ApiUrlFabric).playlist;
}
