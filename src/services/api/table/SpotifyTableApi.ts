import { Either } from '@sweet-monads/either';
import { SpotifyError } from '../../../types/SpotifyTypes';
import { AxiosApi } from '../base/AxiosApi';

export abstract class SpotifyTableApi<T> extends AxiosApi {
	protected abstract _tableUrl: string;

	public getTable(limit = 50, offset = 0): Promise<Either<SpotifyError, SpotifyApi.PagingObject<T>>> {
		const req = this._get<SpotifyApi.PagingObject<T>>({ url: this._getTableWithParams(limit, offset) });

		return this._doApiRequest(req);
	}

	protected _getTableWithParams(limit: number, offset: number): string {
		return `${this._tableUrl}?limit=${limit}&offset=${offset}`;
	}
}
