import { injectable } from 'inversify';
import { getContainer } from '../../../inversify/inversify.getContainer';
import { Types } from '../../../inversify/inversify.types';
import { ApiUrlFabric } from '../ApiUrlFabric';
import { SpotifyTableApi } from './SpotifyTableApi';

@injectable()
export class SpotifySavedTrackListTableApi extends SpotifyTableApi<SpotifyApi.SavedTrackObject> {
	protected _tableUrl = getContainer<ApiUrlFabric>(Types.ApiUrlFabric).savedTracks;
}
