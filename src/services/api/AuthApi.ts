import { injectable } from 'inversify';
import { v4 } from 'uuid';
import { DEV_REDIRECT_PATH } from '../../routing/constants';
import { AxiosApi } from './base/AxiosApi';

@injectable()
export class AuthApi extends AxiosApi {
	public authorize(): void {
		const url = this.getSpotifyAuthUrl();
		window.location.replace(url);
	}

	private getSpotifyAuthUrl(): string {
		const clientId = '4efb79bac4364f0cb8f3afc77d820057';
		const responseType = 'token';
		const redirectUri = encodeURI(process.env.NODE_ENV === 'development' ? DEV_REDIRECT_PATH : '');
		const state = this._spotifyLocalStorageService.getSpotifyAuthState() || v4();
		const scope = 'streaming user-read-private user-read-email user-library-read user-library-modify';

		return `${this._apiUrlFabric.authorize}?client_id=${clientId}&response_type=${responseType}&redirect_uri=${redirectUri}&state=${state}&scope=${scope}`;
	}
}
