import { injectable } from 'inversify';

@injectable()
export class ApiUrlFabric {
	private readonly mainAccountsServiceUrl = 'https://accounts.spotify.com';
	private readonly mainApiUrl = 'https://api.spotify.com/v1';

	public get authorize(): string {
		return `${this.mainAccountsServiceUrl}/authorize`;
	}

	// ===

	public get me(): string {
		return `${this.mainApiUrl}/me`;
	}

	public get playlist(): string {
		return `${this.me}/playlists`;
	}

	public get savedAlbums(): string {
		return `${this.me}/albums`;
	}

	public get savedTracks(): string {
		return `${this.me}/tracks`;
	}

	// ===

	public play(deviceId: string): string {
		return `${this.me}/player/play?device_id=${deviceId}`;
	}
}
