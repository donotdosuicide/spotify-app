import { inject, injectable } from 'inversify';
import { Types } from '../inversify/inversify.types';
import { SpotifyPlayerApi } from './api/SpotifyPlayerApi';
import { SpotifyLocalStorageService } from './SpotifyLocalStorageService';

@injectable()
export class SpotifyWebSdk {
	@inject(Types.SpotifyLocalStorageService) private _spotifyLocalStorageService!: SpotifyLocalStorageService;
	@inject(Types.SpotifyPlayerApi) private _spotifyPlayerApi!: SpotifyPlayerApi;

	private _player: Spotify.Player | undefined = undefined;
	private _isReady = false;

	public get isReady(): boolean {
		return this._isReady;
	}

	public get player(): Spotify.Player | undefined {
		return this._player;
	}

	public async init(): Promise<void> {
		const player = new Spotify.Player({
			name: 'Web Playback SDK Quick Start Player',
			getOAuthToken: (cb) => this.getOAuthToken(cb),
		});

		this.addListeners(player);

		await player.connect();
	}

	public clear(): void {
		this._player && this.removeListeners(this._player);
	}

	public play(data: string[] | string): void {
		const partialPlayData: SpotifyApi.PlayParameterObject = Array.isArray(data)
			? { uris: data }
			: { context_uri: data };

		this._spotifyPlayerApi.play({
			device_id: this._spotifyLocalStorageService.getSpotifyDeviceId() || '',
			...partialPlayData,
		});
	}

	private addListeners(player: Spotify.Player): void {
		player.addListener('initialization_error', this.logPlayerError);
		player.addListener('authentication_error', this.logPlayerError);
		player.addListener('account_error', this.logPlayerError);
		player.addListener('playback_error', this.logPlayerError);

		player.addListener('player_state_changed', (state) => {
			console.log(state);
		});

		player.addListener('ready', ({ device_id }) => {
			console.log('Ready with Device ID', device_id);
			this._spotifyLocalStorageService.setSpotifyDeviceId(device_id);
			this._isReady = true;
			this._player = player;
		});

		player.addListener('not_ready', ({ device_id }) => {
			console.log('Device ID has gone offline', device_id);
		});
	}

	private logPlayerError(data: { message: string }): void {
		console.error(data.message);
	}

	private getOAuthToken(callback: (token: string) => void): void {
		const token = this._spotifyLocalStorageService.getSpotifyAuthAccessToken();

		if (token) {
			callback(token);
		}
	}

	private removeListeners(player: Spotify.Player): void {
		player.removeListener('initialization_error');
		player.removeListener('authentication_error');
		player.removeListener('account_error');
		player.removeListener('playback_error');
		player.removeListener('player_state_changed');
		player.removeListener('ready');
		player.removeListener('not_ready');
	}
}
